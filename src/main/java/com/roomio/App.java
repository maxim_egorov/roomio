package com.roomio;

import com.roomio.utils.GreyScaleUtil;
import com.roomio.utils.ImageUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static class ImagePanel extends JPanel {

        private final BufferedImage image;

        public ImagePanel(String imageFileName) throws IOException {
            final BufferedImage originalImage = ImageIO.read(new File(imageFileName));
            CannyEdgeDetector detector = new CannyEdgeDetector(originalImage, 100d, 10d);
//            image = detector.filter();
            final byte[] greyscale = GreyScaleUtil.filter(originalImage);
            final BufferedImage bufferedImage = ImageUtil.createBufferedImage(greyscale, originalImage.getWidth(), originalImage.getHeight());
            ColorConvertOp grayscale = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);

//            image = grayscale.filter(originalImage, null);
            image = bufferedImage;
            setAutoscrolls(true);
            setPreferredSize(new Dimension(image.getWidth(),image.getHeight()));
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, 0, 0, null);
        }
    }

    public static void main( String[] args ) throws IOException {
//        final ImagePanel imagePanel = new ImagePanel("/home/maxim/wall2.jpg");
//        final ImagePanel imagePanel = new ImagePanel("/home/maxim/sample1.jpg");
        final ImagePanel imagePanel = new ImagePanel("/home/maxim/sample1_1.jpg");
        JFrame f = new JFrame("Image Viewer");
        // TODO Fix kludge to kill the Timer
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

//        final JScrollPane scrollPane = new JScrollPane(imagePanel);
//        scrollPane.setPreferredSize(new Dimension(2000, 3000));
//        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
//        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);


//        JPanel test = new JPanel();
//        test.setPreferredSize(new Dimension( 2000,2000));

//        imagePanel.setPreferredSize(new Dimension( 2000,2000));

        JScrollPane scrollFrame = new JScrollPane(imagePanel);
        scrollFrame.setPreferredSize(new Dimension( 1000,750));

        f.setContentPane(scrollFrame);

//        f.pack();
        f.setSize(1000, 750);
        f.setLocationByPlatform(true);
        f.setVisible(true);
    }
}

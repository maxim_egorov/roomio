package com.roomio.utils;

import java.awt.image.BufferedImage;

public final class GreyScaleUtil {

    private GreyScaleUtil() {
    }

    public static byte[] filter(BufferedImage image) {

        checkIf24RGB(image);
        final int[] pixels = ImageUtil.getPixels(image);
        return filter(pixels);

    }

    public static byte[] filter(int[] original) {
        final byte[] grayScale = new byte[original.length / 3];
        for (int i = 0; i < grayScale.length; i++) {
            final int offset = i * 3;
            final int average = (original[offset] + original[offset + 1] + original[offset + 2]) / 3;
            grayScale[i] = (byte) average;
        }
        return grayScale;
    }

    private static void checkIf24RGB(BufferedImage image) {
        if (image.getData().getNumBands() != 3) {
            throw new IllegalArgumentException("Specified image should have 3 bytes per pixel. " +
                    "Detected: " + image.getData().getNumBands() + " bytes per pixel");
        }
    }
}

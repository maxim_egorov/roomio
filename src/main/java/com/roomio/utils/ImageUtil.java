package com.roomio.utils;

import java.awt.image.BufferedImage;

public final class ImageUtil {
    private ImageUtil() {
    }

    public static int[] getPixels(BufferedImage image) {
        final int width = image.getWidth();
        final int height = image.getHeight();
        return image.getRaster().getPixels(0, 0, width, height, new int[image.getData().getNumBands() * width * height]);
    }

    public static BufferedImage createBufferedImage(byte[] pixels, int width, int height) {
        final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        for(int x = 0; x < width; x ++){
            for(int y=0; y< height; y++){
                int rgb = 256 + pixels[width * y + x];
                image.setRGB(x, y, rgb);
            }
        }

        return image;
    }
}

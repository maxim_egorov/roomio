package com.roomio;


import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class CannyEdgeDetectorTest {

    public static final double THRESHOLD_HIGH = 100d;
    public static final double THRESHOLD_LOW = 10d;

    @Test
    public void testCorrectness() throws IOException, InterruptedException {

        final BufferedImage originalImage = getTestImage();
        final CannyEdgeDetector cannyEdgeDetector = new CannyEdgeDetector(originalImage, THRESHOLD_HIGH, THRESHOLD_LOW);

        final BufferedImage filtered = cannyEdgeDetector.filter();

        final net.semanticmetadata.lire.imageanalysis.filters.CannyEdgeDetector cannyEdgeDetector2 =
                new net.semanticmetadata.lire.imageanalysis.filters.CannyEdgeDetector(originalImage, THRESHOLD_HIGH, THRESHOLD_LOW);
        final BufferedImage filtered2 = cannyEdgeDetector2.filter();

        final int[] pixels = getPixels(filtered);
        final int[] pixels2 = getPixels(filtered2);
        assertTrue(Arrays.equals(pixels2, pixels));

    }


    @Test(timeout = 6000)
    public void testPerformance() throws IOException {

        final BufferedImage originalImage = getTestImage();
        final CannyEdgeDetector cannyEdgeDetector = new CannyEdgeDetector(originalImage, THRESHOLD_HIGH, THRESHOLD_LOW);

        final long startTime = System.currentTimeMillis();
        for (int i = 0; i <= 25; i++) {
            cannyEdgeDetector.filter();
        }

        final long endTime = System.currentTimeMillis();
        System.out.println("Milliseconds: " + (endTime - startTime));
    }

    @Test
    public void testGreyScaleTest() throws IOException {
//        final ColorConvertOp grayscale = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        final BufferedImage originalImage = getTestImage();

        final int width = originalImage.getWidth();
        final int height = originalImage.getHeight();

        final int[] pixels = getPixels(originalImage);

        final long startTime = System.currentTimeMillis();
//        for (int i = 0; i <= 25; i++) {
//            grayscale.filter(originalImage, null);
//        }

        final long endTime = System.currentTimeMillis();
        System.out.println("Milliseconds: " + (endTime - startTime));
    }

    private int[] getPixels(BufferedImage image) {
        final int width = image.getWidth();
        final int height = image.getHeight();
        return image.getRaster().getPixels(0, 0, width, height, new int[width * height]);
    }

    private BufferedImage getTestImage() throws IOException {
        return ImageIO.read(new File("/home/maxim/sample1_1.jpg"));
    }
}

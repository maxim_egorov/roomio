package com.roomio.utils;


import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static junit.framework.Assert.assertTrue;

public class GreyScaleUtilTest {

    @Test
    public void testFilterPerformance() throws IOException {
        final BufferedImage image = getTestImage();
        final int[] original = ImageUtil.getPixels(image);

        final long startTime = System.currentTimeMillis();

        //test filter for 25 frames
        for (int i = 0; i <= 25; i++) {
            GreyScaleUtil.filter(original);
        }
        final long endTime = System.currentTimeMillis();
        final long length = endTime - startTime;

        System.out.println("Milliseconds: " + length);

        assertTrue(length < 100);
    }

    private BufferedImage getTestImage() throws IOException {
        return ImageIO.read(new File("/home/maxim/sample1_1.jpg"));
    }
}

package com.roomio.utils;

import com.roomio.ImageUtils;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;

/**
 * Created by maxim on 21.12.13.
 */
public class GaussianUtilTest {
    @Test
    public void test() throws IOException {
        ConvolveOp gaussian = new ConvolveOp(new Kernel(5, 5, ImageUtils.makeGaussianKernel(5, 1.4f)));
        final long startTime = System.currentTimeMillis();

        //test filter for 25 frames
        for (int i = 0; i <= 25; i++) {
            final BufferedImage filter = gaussian.filter(getTestImage(), null);
        }


        final long endTime = System.currentTimeMillis();
        final long length = endTime - startTime;

        System.out.println("Milliseconds: " + length);
    }

    private BufferedImage getTestImage() throws IOException {
        return ImageIO.read(new File("/home/maxim/sample1_1.jpg"));
    }
}
